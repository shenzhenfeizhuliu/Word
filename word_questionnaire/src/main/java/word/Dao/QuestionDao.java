package word.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import word.Entity.Question;

import java.util.List;

/*
    负责问题实体类Question及对应数据表problem的操作
 */

public interface QuestionDao extends JpaRepository<Question,Integer> {
    public List<Question> findAllByQuestionnaireId(int questionnaireId);

    //更新问题
    @Query("update Question set quesName = ?1 , quesAns = ?2 where quesId = ?3")
    @Modifying
    public void updateData(String quesName, String quesAns, int quesId);
    //删除问题
    @Query("delete from Question where quesId = ?1")
    @Modifying
    public void deleteProblem(int quesId);
    //删除问卷中的问题
    @Query("delete from Question where questionnaireId = ?1")
    @Modifying
    public void dropNaireQuestion(int questionnaireId);
    //更新v1:添加新方法:根据问卷Id按序号输出该问卷下所有问题
    public List<Question> findAllByQuestionnaireIdOrderBySerialNum(int questionnaireId);
}
