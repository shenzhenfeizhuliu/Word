package word.Service;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import word.Dao.QuestionDao;
import word.Dao.QuestionnaireDao;
import word.Entity.Question;
import word.Entity.Questionnaire;

@Service
@Transactional
public class QnaireServiceImpl implements QnaireService{
    @Autowired
    QuestionDao questionDao;
    @Autowired
    QuestionnaireDao questionnaireDao;
    //创建一张空白问卷
    @Override
    public void CreatedQuestionnaire(int projectId, String questionnaireName) {
        Questionnaire data=new Questionnaire();
        data.setProjectId(projectId);
        data.setTitle(questionnaireName);
        try{
            questionnaireDao.save(data);
            //System.out.println("创建完毕,项目ID:"+projectId+",问卷名称:"+questionnaireName);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    //读取问卷表中的所有问卷信息
    @Override
    public List<Questionnaire> LoadAllQuestionnaire() {
        try{
            List<Questionnaire> datalist=questionnaireDao.findAll();
            return datalist;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }
    //修改问卷标题
    @Override
    public void SaveNewQuestionnaireTitle(int questionnaireId, String newQuestionnaireTitle) {
        try{
            questionnaireDao.updateTitle(newQuestionnaireTitle,questionnaireId);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    //根据问题Id删除指定的问题(删除单个问题)
    @Override
    public void DeleteProblem(int[]  problemId) {
        try{
            for (int id:problemId
                 ) {
                questionDao.deleteProblem(id);
            }
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    //删除问卷以及跟该问卷相关的问题
    @Override
    public void DropQuestionnaire(int questionnaireId) {
        try
        {
            questionnaireDao.dropQuestionnaire(questionnaireId);
            questionDao.dropNaireQuestion(questionnaireId);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    //更新v1:修改原有方法public Questionnaire LoadQuestionnaire(int questionnaireId),复制后直接替换即可:读取指定的问卷表
    @Override
    public Questionnaire LoadQuestionnaire(int questionnaireId) {
        try
        {
            List<Question> questionList=questionDao.findAllByQuestionnaireIdOrderBySerialNum(questionnaireId);
            Questionnaire data=questionnaireDao.findByQuestionnaireId(questionnaireId);
            data.setQuestionList(questionList);
            return data;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }
    //更新v1:添加新方法:通过项目Id读取问卷所有信息(问卷信息包含问卷ID,问卷标题和项目ID)
    @Override
    public List<Questionnaire> LoadQuestionnairesByProjectId(int projectId) {
        try {
            List<Questionnaire> questionnaireList=questionnaireDao.findAllByProjectId(projectId);
            return questionnaireList;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    //更新v1:添加新方法:读入一张Excel表生成问卷及问题
    @Override
    public int BuildQuestionnaireByExcel(int projectId,String filePath) {
        try {
            //File file=new File("E:\\Test1.xlsx");//本地测试
            File file=new File(filePath);
            //获得一个输入流
            BufferedInputStream inputStream=new BufferedInputStream(new FileInputStream(file));
            //打开
            XSSFWorkbook workbook=new XSSFWorkbook(inputStream);//XSSFWorkbook适用于2007以上版本的EXCEL表格
            //HSSFWorkbook workbook=new HSSFWorkbook(fileSystem);//HSSFWorkbook用于读取2003以下版本的EXCEL表格
            //创建问卷
            for(int sheetIndex=0;sheetIndex<workbook.getNumberOfSheets();sheetIndex++)
            {
                String title=workbook.getSheetName(sheetIndex);//获取第sheetIndex张sheet的标题
                CreatedQuestionnaire(projectId, title);//根据项目ID和sheet名创建问卷
                //输出测试
                //System.out.println("sheet名:"+title);//输出该sheet的名字
                //System.out.println(data.toString());//问卷信息
            }
            //创建问题
            for(int sheetIndex=0;sheetIndex<workbook.getNumberOfSheets();sheetIndex++)
            {
                Questionnaire questionnaire=questionnaireDao.findByProjectIdAndTitle(projectId,workbook.getSheetName(sheetIndex));
                //获取该sheet所有项
                XSSFSheet sheet=workbook.getSheetAt(sheetIndex);
                //逐行获取数据,不获取作为标题的第0行
                for(int rowIndex=1;rowIndex<=sheet.getLastRowNum();rowIndex++)
                {
                    XSSFRow row =sheet.getRow(rowIndex);//获取第rowIndex行的数据
                    //创建问题
                    Question question=new Question();
                    question.setQuestionnaireId(questionnaire.getQuestionnaireId());
                    question.setSerialNum(row.getCell(0).toString());
                    question.setQuesName(row.getCell(1).toString());
                    questionDao.save(question);
                    //输出测试
                    //System.out.print(row.getCell(0)+",");//打印第rowIndex行的第0个单元格
                    //System.out.println(row.getCell(1));//打印第rowIndex行的第1个单元格
                }
            }
            inputStream.close();
            return 1;
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
            return -1;
        }
    }

    //更新v2:修改原有方法:更新问卷
    @Override
    public void SaveNewQuestionnaire(Questionnaire questionnaire) {
        try
        {
            //更新问卷的标题
            questionnaireDao.updateTitle(questionnaire.getTitle(),questionnaire.getQuestionnaireId());
            List<Question> dataList=questionnaire.getQuestionList();
            for (Question q:dataList
                 ) {
                //更新问题的字段以及字段内容
                questionDao.updateData(q.getQuesName(),q.getQuesAns(),q.getQuesId());
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    //更新v2:添加新方法:读入项目id和excel表创建问卷
    @Override
    public int BuildQuestionnaireByExcel(int projectId, MultipartFile multipartFile) {
        try {
            String filename=multipartFile.getOriginalFilename();//获取文件名xxxxx.xxx
            String eName=filename.substring(filename.lastIndexOf(".")+1);//获取文件拓展名.xxx
            //System.out.println("接收到数据,项目id:"+projectId+",文件:"+filename+",格式:"+eName);
            if(eName.equals("xlsx") || eName.equals("xls"))
            {
                //获得一个输入流
                BufferedInputStream inputStream= new BufferedInputStream(multipartFile.getInputStream());
                //打开
                XSSFWorkbook workbook=new XSSFWorkbook(inputStream);
                //创建问卷
                for(int sheetIndex=0;sheetIndex<workbook.getNumberOfSheets();sheetIndex++)
                {
                    String title=workbook.getSheetName(sheetIndex);//获取第sheetIndex张sheet的标题
                    CreatedQuestionnaire(projectId, title);//根据项目ID和sheet名创建问卷
                    //输出测试
                    //System.out.println("sheet名:"+title);//输出该sheet的名字
                }
                //创建问题
                for(int sheetIndex=0;sheetIndex<workbook.getNumberOfSheets();sheetIndex++)
                {
                    Questionnaire questionnaire=questionnaireDao.findByProjectIdAndTitle(projectId,workbook.getSheetName(sheetIndex));
                    //获取该sheet所有项
                    XSSFSheet sheet=workbook.getSheetAt(sheetIndex);
                    //逐行获取数据,不获取作为标题的第0行
                    for(int rowIndex=1;rowIndex<=sheet.getLastRowNum();rowIndex++)
                    {
                        XSSFRow row =sheet.getRow(rowIndex);//获取第rowIndex行的数据
                        //创建问题
                        Question question=new Question();
                        question.setQuestionnaireId(questionnaire.getQuestionnaireId());
                        question.setSerialNum(row.getCell(0).toString());
                        question.setQuesName(row.getCell(1).toString());
                        questionDao.save(question);
                        //输出测试
                        //System.out.print(row.getCell(0)+",");//打印第rowIndex行的第0个单元格
                        //System.out.println(row.getCell(1));//打印第rowIndex行的第1个单元格
                    }
                }
                inputStream.close();
                return 1;
            }else
            {
                return -2;
            }
        }catch (Exception e)
        {
            return -1;
        }
    }
}
