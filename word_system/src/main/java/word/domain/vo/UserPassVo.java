package word.domain.vo;

import lombok.Data;

/**
 * Created By IntelliJ IDEA.
 * Author: AL-
 * Date: 2020/01/06 16:32
 */
@Data
public class UserPassVo {

    private String oldPass;

    private String newPass;
}
