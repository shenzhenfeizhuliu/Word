package word.domain;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created By IntelliJ IDEA.
 * Author: AL-
 * Date: 2020/01/06 16:32
 */
@Entity
@Getter
@Setter
@Table(name = "log")
@NoArgsConstructor
public class Log  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",insertable = false,updatable = false)
    private Long id;

    // 操作用户
    private String username;

    // 描述
    private String description;

    // 方法名
    private String method;

    // 参数
    @Column(columnDefinition = "text")
    private String params;

    // 日志类型
    @Column(name = "log_type")
    private String logType;

    // 请求ip
    @Column(name = "request_ip")
    private String requestIp;

    @Column(name = "address")
    private String address;

    private String browser;

    // 请求耗时
    private Long time;

    // 异常详细
    @Column(name = "exception_detail", columnDefinition = "text")
    private byte[] exceptionDetail;

    // 创建日期
    @CreationTimestamp
    @Column(name = "create_time")
    private Timestamp createTime;

    public Log(String logType, Long time) {
        this.logType = logType;
        this.time = time;
    }
}
